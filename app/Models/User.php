<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Cache\RedisLock;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redis;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package App\Models
 */
class User extends Model
{

    public $table = 'user';
    private $redis;

    protected $fillable = [
        'name', 'email', 'provider','image','uid'
    ];

    /**
     * User constructor.
     */
    function __construct()
    {
        $this->redis = Redis::connection();
    }

    /**
     * create user in db
     * @param array $data
     * @return array
     */
    public function store(array $data){
        //tomamos la variable que sera el identificador del usuario
//        $user_id = $this->getUserId();
        $user_id = $data['email'];
        //generamos un token unico
        $token = $this->getTokenApp();
        //chequeamos si el parametro uid es el correcto para el usuario(en caso de que este creado)
        if ($this->userExists($user_id)){
            if (!$this->checkuid($user_id, $data['uid'])){
                //devolvemos la respuesta al cliente
                return [
                    'success' => false,
                    'validateuid' => false
                ];
            };
        };


        // creamos o actualizamos el usuario registrado
        $create_user = $this->redis->hmset('user:'. $user_id, [
            'name' => $data['name'],
//            'email' => $data['email'],
            'provider' => $data['provider'],
            'image' => $data['image'],
            'uid' => $data['uid'],
        ]);

        //si se crea el usuario le creamos en bd un identificador para el token generado,de manera que pueda ser validado
        if($create_user){
            $token_user = $this->createToken($user_id, $data['provider'], $token);
            if ($token_user){
                //devolvemos la respuesta al cliente
                return [
                    'success' => true,
                    'data' => [
                        'uid' => $data['uid'],
                        'token' => $token
                    ]
                ];

            }
        }
        //si no creamos el usuario o el identificador del token en bd devolvemos la respuesta de error al cliente
        return ['success' => false];
    }

//    private function getUserId(){
//        return  $this->redis->incr('next_user_id');
//    }

    /**
     * genera un string aleatorio unico
     * @return string
     */
    private function getTokenApp(){
        return (string) Str::uuid();
    }

    /**
     * creamos el identificador del token en bd para el proveedor
     * @param $user_id
     * @param $provider
     * @param $token
     * @return mixed
     */
    private function createToken($user_id, $provider, $token){
       return $this->redis->hmset('user:'. $user_id . ":token", [
           $provider => $token
       ]);
    }

    /**
     * verifica si existe el usuario en bd
     * @param $user_id
     * @return bool
     */
    public function userExists($user_id){
        $check = $this->redis->exists('user:' . $user_id);
        if ($check){
            return true;
        }
        return false;
    }

    /**
     * revisa el valor del uid del usuario
     * @param $user_id
     * @param $uid
     * @return bool
     */
    public function checkuid($user_id, $uid){
        $uid_stored = $this->redis->hget('user:' . $user_id, 'uid');
        if ($uid === $uid_stored){
            return true;
        }
         return false;

    }

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required | regex:/^[A-Za-z\s]+$/',
        'email' => 'required | email',
        'provider' => 'required',
        'image' => 'required | url',
        'uid' => 'required'
    ];

    /**
     * Validation rules messages
     *
     * @var array
     */
    public static $messages = [
        'name.required' => 'El Nombre es requerido',
        'name.regex' => 'El Nombre debe contener solo letras',
        'email.required' => 'El Correo Electronico es requerido',
        'email.email' => 'El Correo Electronico debe ser un Email valido',
        'provider.required' => 'El Proveedor es requerido',
        'image.required' => 'La Imagen es requerida',
        'image.url' => 'La Imagen debe ser una url valida',
        'uid.required' => 'El Identificador es requerido'
    ];
}
