<?php

namespace App\Repositories;

use App\Models\User;
use InfyOm\Generator\Common\BaseRepository;

class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    public function store(array $data)
    {
        $user = new User();
       return $user->store($data);
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }
}
