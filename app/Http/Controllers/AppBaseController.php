<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use InfyOm\Generator\Utils\ResponseUtil;
use App\Http\Controllers\Controller as LaravelController;

/**
 * @SWG\Swagger(
 *   basePath="/api/v1",
 *   @SWG\Info(
 *     title="Laravel Generator APIs",
 *     version="1.0.0",
 *   )
 * )
 * This class should be parent class for other API controllers
 * Class AppBaseController
 */
class AppBaseController extends LaravelController
{
    public function sendResponse($result, $message)
    {
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    public function sendError($error_message, $code = 500)
    {
        return Response::json(ResponseUtil::makeError($error_message), $code);
    }

    public function sendErrorValidation($messages, $code = 400){
        return Response::json(ResponseUtil::makeError('Failed Validation', $messages->toArray()), $code);
    }
}
