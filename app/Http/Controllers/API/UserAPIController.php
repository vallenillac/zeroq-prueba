<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUserAPIRequest;
use App\Http\Requests\API\UpdateUserAPIRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class UserAPIController extends InfyOmBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    /**
     * UserAPIController constructor.
     * @param UserRepository $userRepo
     */
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Guardamos un usuario en la bd segun los datos recibidos
     * POST /users
     *
     * @param CreateUserAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateUserAPIRequest $request)
    {
        //traemos todos los campos de la variable
        $input = $request->all();

        //guardamos en db, si existe el registro, actualiza
        $user = $this->userRepository->store($input);

        //devolvemos la respuesta al cliente dependiendo de la situacion
        if ($user['success'] == true){
            return $this->sendResponse($user, 'Usuario creado satisfactoriamente');
        }elseif($user['success'] == false && $user['validateuid'] == false){
            return $this->sendError('uid no valido, por favor ingrese el correcto', 400);
        }else{
            return $this->sendError('Error al crear el Usuario');
        }

    }
}
