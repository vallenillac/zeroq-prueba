<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ZeroQ</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
       <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-dark bg-dark">
            <span class="navbar-brand mb-0 h1">ZeroQ</span>
        </nav>
        <div class="container">
            <div class="mx-auto col-md-6 pt-4">
                <h2>Registrar Usuario</h2>
                {{Form::open(['url' => 'api/users', 'method' => 'Post'])}}
                <div class="form-group">
                    {{Form::label('name', 'Nombre')}}
                    {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Juan Perez']) }}
                </div>
                <div class="form-group">
                    {{Form::label('email', 'Correo Electronico')}}
                    {{ Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'mymail@mailer.com']) }}
                </div>
                <div class="form-group">
                    {{Form::label('provider', 'Proveedor')}}
                    {{ Form::text('provider', null, ['class' => 'form-control', 'placeholder' => 'Mi Proveedor']) }}
                </div>
                <div class="form-group">
                    {{Form::label('uid', 'uid')}}
                    {{ Form::text('uid', null, ['class' => 'form-control', 'placeholder' => 'a34fTe223wq']) }}
                </div>
                <div class="form-group">
                    {{Form::label('image', 'Imagen')}}
                    {{ Form::text('image', null, ['class' => 'form-control', 'placeholder' => 'http://url-to-image.png']) }}
                </div>
                <button class="btn btn-block btn-primary" type="submit">Registrarme</button>
                {{Form::close()}}
            </div>
        </div>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">


                {{--<div class="links">--}}
                    {{--<a href="https://laravel.com/docs">Documentation</a>--}}
                    {{--<a href="https://laracasts.com">Laracasts</a>--}}
                    {{--<a href="https://laravel-news.com">News</a>--}}
                    {{--<a href="https://forge.laravel.com">Forge</a>--}}
                    {{--<a href="https://github.com/laravel/laravel">GitHub</a>--}}
                {{--</div>--}}
            </div>
        </div>
    </body>
    <!-- Javascript-->
    <script src="{{ asset('js/app.js') }}"></script>
</html>
