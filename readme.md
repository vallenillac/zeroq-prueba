﻿## ZeroQ Prueba Técnica

### Acerca de la aplicación
Es una aplicación web tipo rest desarrollada en php,utilizando el framework Laravel en su ultima versión.

La Estructura para revisar el desarrollo realizado es la siguiente:
* app/Http/Controllers/API -> controladores
* app/Http/Requests/API -> form requests (en laravel a través de los requests podemos controlar la información recibida en los requests antes de pasarla al controlador)
* app/Models ->  modelos 
* app/Repositories ->  modelos 
* resources/views/welcome.blade.php -> vista generada con formulario de registro con los parámetros solicitados en el requerimiento

### Notas acerca del desarrollo

Al desarrollar la prueba tuve ciertas dudas con el caso, como por ejemplo, _para que es el token?_ o _ para que es el uid?_ por lo que investigando un poco me fui a la url base de la prueba [ZeroQ](http://dev.zeroq.cl)
y vi una aplicación para la gestion de filas, __por lo que en el desarrollo intento de utilizar la lógica como si fuera para esa aplicación.__

Sabiendo esto, indico algunos ítems a tomar en cuenta cuando revisen el desarrollo:
* Envío de imagen: para el parámetro de imagen registro la variable como una url, podría ser operada también con alguna codificación como base64 pero como no es especificado en el requerimiento lo coloque como una url
* uno de los requerimientos dice: 
    > Se minimice el número de tokens para un mismo usuario.

    Por lo que la lógica utilizada es crear un hashset de tokens para el usuario, pero el token es para cada proveedor (suponiendo que el proveedor en la aplicación sea una empresa que se le asignara un espacio en la fila y el token es ese identificador del usuario en la fila) y asi se puede evitar la creación infinita de tokens cada vez que se haga un requests, porque si ya para el proveedor existe un token este sera devuelto sino se crea.

* Identificador por email en base al pseudocódigo: el pseudocódigo indica que si pasa la validación se revise en base de datos si existe el usuario y luego revise los uid, por lo que en base de datos coloque de identificador el email que puede ser un valor único

### Acceso y uso de a la aplicación

Puede utilizar el formulario el cual se encuentra en el siguiente enlace:

   [Formulario](http://190.96.15.4/zeroq-prueba/public)
   
Sino puede utilizar un cliente rest en el siguiente enlace:

[http://190.96.15.4/zeroq-prueba/public/api/users](http://190.96.15.4/zeroq-prueba/public/api/users)

Haciendo una petición por POST con los siguientes parametros:

```javascript
Header: Content-Type:application/json
{
         "uid"      : "lho9gjm",
         "email"    : "cesar@mail.com",
         "name"     : "Cesar",
         "provider" : "proveedor1",
         "image"    : "http://image.png"
 }
 ```
 
 __Cualquier de las 2 vías mencionadas devolverán la respuesta de la api en formato json__
 
 
### Alcances, Limitaciones y Posibles Mejoras planificadas

En el texto de la prueba mencionan cambio de bases de datos de redis a MongoDB o Cassandra, esto habría que evaluarlo dependiendo del uso de la aplicación
porque si es una aplicación que va a tener una alta demanda de consultas a la bd y simplicidad de los lo mejor sería redis pero si hay una estructura de datos compleja quiza sea mejor usar mongoDB o cassandra por ejemplo



